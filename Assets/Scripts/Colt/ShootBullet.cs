using UnityEngine;

public class ShootBullet : MonoBehaviour
{
    [SerializeField] float _speed;
    private void Start()
    {
        // destroy bullet with this time
        Destroy(gameObject, 5);
    }
    // move bullet
    void FixedUpdate()
    {
        transform.position += transform.forward * Time.deltaTime * _speed;
    }

}
