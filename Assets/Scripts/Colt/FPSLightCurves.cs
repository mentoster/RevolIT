using UnityEngine;
using System.Collections;

public class FPSLightCurves : MonoBehaviour
{
    public AnimationCurve lightCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public float graphTimeMultiplier = 1, graphIntensityMultiplier = 1;

    private bool _canUpdate;
    private float _startTime;
    private Light _lightSource;

    private void Awake()
    {
        _lightSource = GetComponent<Light>();
        _lightSource.intensity = lightCurve.Evaluate(0);
    }

    private void OnEnable()
    {
        _startTime = Time.time;
        _canUpdate = true;
        _lightSource.enabled = true;
    }

    private void Update()
    {
        var time = Time.time - _startTime;
        if (_canUpdate)
        {
            var eval = lightCurve.Evaluate(time / graphTimeMultiplier) * graphIntensityMultiplier;
            _lightSource.intensity = eval;
        }

        if (time >= graphTimeMultiplier)
        {
            _canUpdate = false;
            _lightSource.enabled = false;
        }
    }
}