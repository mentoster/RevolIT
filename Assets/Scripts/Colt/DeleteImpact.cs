using UnityEngine;

public class DeleteImpact : MonoBehaviour
{
    private void Start()
    {
        Destroy(gameObject, 20);
    }
}
