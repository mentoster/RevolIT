
using System.Collections;
using System.Collections.Generic;
using Bhaptics.Tact.Unity;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR;
using Valve.VR.InteractionSystem;


public class LocalColt : MonoBehaviour
{
    [Header("GameObjects")]
    [SerializeField] Transform _shootPoint;
    [SerializeField] GameObject _shootPrefab;
    [SerializeField] Transform _trigger;
    [SerializeField] Drum _drum;
    int _scores = 0;
    [SerializeField] TMPro.TMP_Text _scoresText;
    Transform _drumOTransform;
    [Header("Gun settings")]
    [SerializeField] float _fireRate = 0.3f;
    float _shootAnimationSpeed;
    byte _maxBullets;
    public byte bullets = 6;
    [Range(0, 20)]

    readonly int _damage = 30;
    bool _canShoot = true;
    [SerializeField] float _reloadTime;
    AudioSource _audioSource;
    [Header("Sounds")]
    [SerializeField] AudioClip[] _shootSounds;
    AudioClip _shootSound;
    [SerializeField] AudioClip _shootToEnemy;

    [SerializeField] AudioClip _emptySound;
    [Header("Effects")]
    [SerializeField] GameObject _shootEffect;
    Dictionary<string, GameObject> _effects = new Dictionary<string, GameObject>();
    [SerializeField] List<string> _effectsName;
    [SerializeField] List<GameObject> _effect;

    // bhaptic
    [Header("Vr settings")]
    [SerializeField] SteamVR_Action_Boolean _fireAction;
    [SerializeField] SteamVR_Action_Boolean _reloadAction;

    Sequence _mySequence;
    Interactable _interactable;
    Throwable _throwable;
    BhapticConnect _bhapticConnect;
    public int coltIndex = 0;
    LeaderBoard _lb;
    string[] _playersNames = new string[4];
    string[] _playersBaseNames =
    {
             "Первый_экспертный",
             "Второй_смертельный",
             "Третий_бессмертный",
             "Четвертый_трезвый",
        };
    bool _vibration = false;
    [SerializeField] private bool _isBot = false;

    private void Start()
    {
        for (int i = 0; i < 4; i++)
        {
            _playersNames[i] = $"{_playersBaseNames[i]}_#{Random.Range(0, 10000)}";
        }
        coltIndex = 0;
        print($"74.  Colt -> playersNames : {_playersNames[coltIndex]}");
        _mySequence = DOTween.Sequence();
        _maxBullets = bullets;
        _audioSource = GetComponent<AudioSource>();
        _shootSound = _shootSounds[Random.Range(0, _shootSounds.Length)];
        _bhapticConnect = GetComponent<BhapticConnect>();
        _bhapticConnect.shootingPoint = _shootPoint;
        _interactable = GetComponent<Interactable>();
        _throwable = GetComponent<Throwable>();
        _shootAnimationSpeed = _fireRate / 2;
        _drumOTransform = _drum.gameObject.transform;
        //  show player name
        // turn on text on pistol
        _scoresText.gameObject.transform.localPosition = new Vector3(-0.00028f, 0, 0.001414f);
        _scoresText.fontSize = 8;
        _scoresText.gameObject.SetActive(true);
        _scoresText.text = "Ваше имя:\n" + _playersNames[coltIndex];
        StartCoroutine(HideName());
        _lb = GameObject.Find("leaderboards").GetComponent<LeaderBoard>();
        if (_audioSource == null)
        {
            Debug.LogError("No audiosource!");
        }
        if (_effectsName.Count == _effect.Count)
        {
            for (int i = 0; i < _effectsName.Count; ++i)
                _effects.Add(_effectsName[i], _effect[i]);
        }
        else
        {
            Debug.LogError($"Error, effectsName and effects must be same number! {_effectsName.Count} !={ _effect.Count} ");
        }
    }


    private void Update()
    {
        if (_canShoot != true || _isBot)
        {
            return;
        }
        // if (_interactable.attachedToHand != null)
        // {
        //     SteamVR_Input_Sources source = _interactable.attachedToHand.handType;
        //     if (_fireAction[source].stateDown)
        //     {
        //         Shoot();
        //     }
        //     else if (_reloadAction[source].stateDown)
        //     {
        //         Reload();
        //     }
        // if (vibration)
        // {
        //     _interactable.attachedToHand.TriggerHapticPulse(65000);
        // }
        // }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }

    }


    #region shoot
    /// <summary>
    /// Does not allow you to shoot while taking an colt
    ///</summary>
    public void AllowShoot()
    {
        StartCoroutine(AllowShootTimer());
    }

    public void ConnectToPlayer()
    {
        //  send to player function about detach from hand
        transform.root.GetComponent<Target>().coltDetach = () => Die();
    }

    IEnumerator AllowShootTimer()
    {
        yield return new WaitForSeconds(1f);
        _canShoot = true;
    }

    public void Shoot()
    {

        if (bullets > 0)
        {
            --bullets;
            play_effect();
            // Debug.DrawRay(_shootPoint.position, _shootPoint.forward * 100, Color.red, 1);
            if (Physics.Raycast(_shootPoint.position, _shootPoint.forward * 100, out RaycastHit raycastHit, maxDistance: 1000))
            {
                print($"107. Colt -> shoot at: {raycastHit.transform.gameObject}");
                if (raycastHit.transform.tag == "Player")
                {
                    raycastHit.transform.gameObject.SendMessage("ApplyDamage");
                }
                else
                {
                    Instantiate(_effects["all"], raycastHit.point, Quaternion.LookRotation(raycastHit.normal));
                    Quaternion rot = Quaternion.LookRotation(raycastHit.normal);
                }
            }
            // bullet from colt
            var bulletEffect = Instantiate(_shootPrefab, _shootPoint);
            bulletEffect.transform.parent = null;
            StartCoroutine(ShowJoyStick());
            // animate drum
            _drum.Shoot(bullets, _fireRate);
            StartCoroutine(FireRateTimer());
            _canShoot = false;
        }
        else
        {
            _audioSource.PlayOneShot(_emptySound);
        }
    }

    IEnumerator ShowJoyStick()
    {
        _vibration = true;
        yield return new WaitForSeconds(.300f);
        _vibration = false;


    }

    void sendBhapticHit()
    {
        if (Physics.Raycast(_shootPoint.position, _shootPoint.forward * 100, out RaycastHit raycastHit, maxDistance: 1000))
        {
            if (raycastHit.transform.tag == "Player")
            {
                sendBhaptic(raycastHit);
            }

        }
    }

    void sendBhaptic(RaycastHit raycastHit)
    {
        _bhapticConnect.Play(raycastHit: raycastHit);
    }

    public void spawn_impact(float x, float y, float z, float rot_x, float rot_y, float rot_z, float rot_w)
    {
        Instantiate(_effect[0], new Vector3(x, y, z), new Quaternion(rot_x, rot_y, rot_z, rot_w));
    }

    public void play_effect()
    {
        _audioSource.PlayOneShot(_shootSound);
        // particle effects
        _shootEffect.SetActive(false);
        _shootEffect.SetActive(true);
        // Trigger and drum effect
        ActivateTrigger();
        _drum.Shoot(bullets, _fireRate);
    }

    IEnumerator FireRateTimer()
    {
        yield return new WaitForSeconds(_fireRate);
        _canShoot = true;
    }

    void ActivateTrigger()
    {
        _trigger.DOLocalRotate(new Vector3(0, 25, 0), _shootAnimationSpeed);
        StartCoroutine(ReturnTrigger());
    }

    IEnumerator ReturnTrigger()
    {
        yield return new WaitForSeconds(_shootAnimationSpeed);
        _trigger.DOLocalRotate(new Vector3(0, 0, 0), _fireRate);
    }

    public void UpdateScore(bool kill, int addScores)
    {
        // make sound about hit to enemy
        _audioSource.PlayOneShot(_shootToEnemy);
        if (kill)
            addScores += 50;

        _scores += addScores;
        // kill last animation
        _mySequence.Kill();
        _mySequence = DOTween.Sequence();
        // set default position and size for text
        _scoresText.gameObject.transform.localPosition = new Vector3(-0.00028f, 0, 0.001414f);
        _scoresText.fontSize = 24;
        // show scores
        _scoresText.gameObject.SetActive(true);
        _scoresText.text = $"Попадание:\n+{addScores}";
        // move scores up and make font smaller
        _mySequence.Append(_scoresText.gameObject.transform.DOLocalMoveZ(0.005f, 2)).Append
        (DOTween.To(() => _scoresText.fontSize, x => _scoresText.fontSize = x, 0, 1.8f));
        // add to record table
        _lb.AddNewHighscore(_playersNames[coltIndex], _scores);
    }

    public void Die()
    {
        DetachFromHand();
    }
    ///<summary>
    ///Detach colt from hand
    ///</summary>
    void DetachFromHand()
    {
        _interactable.attachedToHand.DetachObject(this.gameObject);
        Destroy(_throwable);
        GetComponent<Rigidbody>().isKinematic = false;
        StartCoroutine(TurnOnGravity());
    }
    IEnumerator TurnOnGravity()
    {
        yield return new WaitForSeconds(0.1f);
        GetComponent<Rigidbody>().useGravity = true;
    }

    #endregion

    #region reload
    public void Reload()
    {
        if (bullets != _maxBullets)
        {
            // start ReloadAnimation
            _drum.Open();
            _canShoot = false;
            bullets = _maxBullets;
            StartCoroutine(EndReloadTimer());
        }

    }

    IEnumerator EndReloadTimer()
    {
        yield return new WaitForSeconds(_reloadTime);
        _canShoot = true;
        _drum.Close();
    }
    IEnumerator HideName()
    {
        yield return new WaitForSeconds(18f);
        _scoresText.gameObject.SetActive(false);
    }
    #endregion
}
