﻿using Photon.Pun;
using UnityEngine;

namespace Bhaptics.Tact.Unity
{
    public class BhapticConnect : MonoBehaviour
    {
        HapticSender _tactSender;
        [HideInInspector]
        public Transform shootingPoint;
        PhotonView _pV;
        void Start()
        {
            _tactSender = GetComponent<HapticSender>();
        }

        public void Play(RaycastHit raycastHit)
        {
            var detect = raycastHit.collider.gameObject.GetComponent<HapticReceiver>();
            var pos = detect.PositionTag;
            print(_tactSender);
            if (_tactSender != null && detect != null)
            {
                _tactSender.Play(pos, raycastHit);
            }
        }
    }
}
