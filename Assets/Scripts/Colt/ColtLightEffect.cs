using System.Collections;
using UnityEngine;

public class ColtLightEffect : MonoBehaviour
{
    [SerializeField] private Light _fireLight;

    private void OnEnable()
    {
        _fireLight.enabled = true;
        StartCoroutine(DisableLight());
    }
    IEnumerator DisableLight()
    {
        yield return new WaitForSeconds(0.1f);
        _fireLight.enabled = false;
    }
}
