using DG.Tweening;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Rendering;

public class CameraEffect : MonoBehaviour
{
    [SerializeField] float timeEffect = 4;
    Volume _globalVolume;
    [SerializeField] PhotonView PV;
    private void Start()
    {

        _globalVolume = GameObject.Find("Global Volume").GetComponent<Volume>();
    }

    /// <summary>
    /// Makes the screen gradually gray
    /// </summary>
    public void DieEffect()
    {
        if (PV.IsMine)
        {
            DOTween.To(() => _globalVolume.weight, x => _globalVolume.weight = x, 1, timeEffect);
        }
    }
    /// <summary>
    /// Return colors to screen
    /// </summary>
    public void RespawnEffect()
    {
        if (PV.IsMine)
        {
            DOTween.To(() => _globalVolume.weight, x => _globalVolume.weight = x, 0, timeEffect);
        }

    }
}
