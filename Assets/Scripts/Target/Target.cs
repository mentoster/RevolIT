using System;
using System.Collections;
using Photon.Pun;
using UnityEngine;
using Debug = UnityEngine.Debug;
interface ITarget
{
    Tuple<bool, int> TakeDamage(int damage, int index);
}

public class Target : MonoBehaviour, ITarget
{
    public PhotonView pV;
    [SerializeField] float _health = 100;
    [SerializeField] CameraEffect _cameraEffect;
    [SerializeField] TMPro.TMP_Text _dieText;
    [SerializeField] float _timeForDieText = 10;
    [SerializeField] VRRig _vRRig;
    SceneController _sceneController;
    bool _isDied = false;
    int _id;

    public Action coltDetach;

    private void Start()
    {
        _sceneController = GameObject.Find("SceneController").GetComponent<SceneController>();
        _id = PhotonNetwork.CurrentRoom.PlayerCount - 1;
    }
    [PunRPC]
    public Tuple<bool, int> TakeDamage(int damage, int index)
    {
        print($"14. {gameObject} [{_health}] -> take damage : {damage} -> now have {_health - damage}");
        _health -= damage;
        if (_health < 0)
        {
            Die();
            PhotonView scene_PV = GameObject.Find("SceneController").GetComponent<PhotonView>();
            scene_PV.RPC("dead", RpcTarget.AllBuffered, index);
            return Tuple.Create(true, 0);
        }
        return Tuple.Create(false, damage);
    }

    private void Update()
    {
        // TODO: delete update, i made it for test die effects
        if (Input.GetKeyDown(KeyCode.P))
        {
            Die();
        }
        if (_sceneController.canRestart)
        {
            _ = StartCoroutine(TimerForRestart());
        }
    }

    void Die()
    {
        _isDied = true;
        if (coltDetach != null)
        {
            coltDetach();
        }
        else
        {
            Debug.LogAssertion($" Target -> _colt  is null");
        }
        if (_cameraEffect != null)
        {
            _cameraEffect.DieEffect();
            _ = StartCoroutine(TimerForEnableDieText());
            // disable rigging
            _vRRig.DisableForward();
            // detach colt
        }
        else
        {
            Debug.LogAssertion($"Target -> coltDetach is null, maybe player don't have colt in hands?");
        }
    }
    IEnumerator TimerForRestart()
    {
        yield return new WaitForSeconds(5f);
        _dieText.text = _isDied ? GameControl.loseText : GameControl.winnerText;
        if (pV.IsMine)
        {
            _dieText.gameObject.SetActive(true);
            _ = StartCoroutine(Timer());
            yield return new WaitForSeconds(10 + (_id * 4));
            _sceneController.RestartGame();
        }
    }
    IEnumerator Timer()
    {
        int maxTime = 10 + (_id * 4);
        while (true)
        {
            yield return new WaitForSeconds(1);
            _dieText.text = _dieText.text.Remove(_dieText.text.Length - 1, _dieText.text.Length) + maxTime.ToString();
        }

    }
    IEnumerator TimerForEnableDieText()
    {
        yield return new WaitForSeconds(2f);
        if (pV.IsMine)
        {
            _dieText.gameObject.SetActive(true);
            yield return new WaitForSeconds(_timeForDieText);
            if (!_sceneController.canRestart)
            {
                _dieText.gameObject.SetActive(false);
            }
        }
    }

    public void Respawn()
    {
        if (_cameraEffect != null)
        {
            _cameraEffect.RespawnEffect();
            _vRRig.EnableForward();
        }
        else
        {
            Debug.LogAssertion("47. Target -> _cameraEffect is null");
        }
    }
}
