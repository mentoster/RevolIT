﻿using System;
using Photon.Pun;
using UnityEngine;

public class TargetPart : MonoBehaviour, ITarget
{
    [SerializeField] Target _target;
    [SerializeField] int _bonus = 1;

    public Tuple<bool, int> TakeDamage(int damage, int index)
    {
        _target.pV.RPC("TakeDamage", RpcTarget.All, damage * _bonus, index);
        return Tuple.Create(false, damage * _bonus);
    }
}
