﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.Animations.Rigging;

/// <summary>
/// Matching the VR controllers and headset to their targets
/// </summary>
[System.Serializable]
public class VRMap
{

    public PhotonView PV;
    public Transform vrTarget, rigTarget;
    public bool notHead = true;
    [SerializeField]
    public Vector3 trackingPositionOffSet, trackingRotationOffSet;
    bool usePhysics = false;
    /// <summary>
    /// Sets the position and rotation of the "rig target" to be the "VR target"
    /// </summary>
    public void Mapping()
    {

        if (PV.IsMine || !notHead)
        {
            rigTarget.position = vrTarget.TransformPoint(trackingPositionOffSet);
            // make offset wrong
            // rigTarget.position = vrTarget.position;
            rigTarget.rotation = vrTarget.rotation * Quaternion.Euler(trackingRotationOffSet);
        }

    }
    public void changePhysics()
    {
        usePhysics = !usePhysics;
        if (usePhysics)
        {
            rigTarget.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            rigTarget.gameObject.GetComponent<Collider>().enabled = true;
        }
        else
        {
            rigTarget.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            rigTarget.gameObject.GetComponent<Collider>().enabled = false;
        }

    }
}

public class VRRig : MonoBehaviour
{
    [Header("VR objects")]
    [SerializeField]
    private VRMap head;
    [SerializeField]
    private VRMap leftHand;
    [SerializeField]
    private VRMap rightHand;

    [Space]

    [SerializeField]
    private Transform headConstraint;

    private Vector3 headBodyOffSet;
    [SerializeField]
    private float turnSmoothness;
    bool mustForward = true;
    void Start()
    {
        headBodyOffSet = transform.position - headConstraint.position;
    }
    void LateUpdate()
    {
        if (mustForward)
        {
            transform.position = headConstraint.position + headBodyOffSet;
            transform.forward = Vector3.Lerp(transform.forward,
                Vector3.ProjectOnPlane(headConstraint.up, Vector3.up).normalized, Time.deltaTime * turnSmoothness);
            head.Mapping();
            leftHand.Mapping();
            rightHand.Mapping();
        }

    }

    public void DisableForward()
    {
        if (mustForward)
        {
            GetComponent<Rigidbody>().isKinematic = false;
            ChangePhysics();
            mustForward = false;
        }
    }
    public void EnableForward()
    {
        if (!mustForward)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            ChangePhysics();
            mustForward = true;
        }
    }
    void ChangePhysics()
    {
        head.changePhysics();
        head.rigTarget.gameObject.GetComponent<MultiParentConstraint>().weight = mustForward ? 0 : 1;
        leftHand.changePhysics();
        rightHand.changePhysics();
    }

    // void LateUpdate()
    // {
    //     PV.RPC("sync_pos", RpcTarget.All, headConstraint.position.x, headConstraint.position.y, headConstraint.position.z, headConstraint.rotation.);
    // }

    // public void sync_pos(float x, float y, float z, float rot_x, float rot_y, float rot_z, float rot_w)
    // {
    //     transform.position = new Vector3(x, y, z) + headBodyOffSet;
    //     Transform
    //     Vector3 vec = new Vector3(x, y, z);
    //     transform.forward = Vector3.Lerp(transform.forward,
    //         Vector3.ProjectOnPlane(vec.u, Vector3.up).normalized, Time.deltaTime * turnSmoothness);

    //     head.Mapping();
    //     leftHand.Mapping();
    //     rightHand.Mapping();
    // }
}
