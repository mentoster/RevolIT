using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(AudioSource))]
public class StartDuel : MonoBehaviour
{

    [Header("Visual")]

    [SerializeField] private TMP_Text _timerText;
    [SerializeField] private TMP_Text _battleText;


    [Header("Audio")]
    [SerializeField] private AudioClip _tickClip;
    [SerializeField] private AudioClip _startBattleClip;
    private AudioSource _audiosource;


    [Header("Other Settings")]
    [SerializeField] private int _duelStartTimer = 6;

    [SerializeField] private bool _isLocal = false;
    [SerializeField] private GameObject _stopWall;
    private void Start()
    {
        _audiosource = GetComponent<AudioSource>();

        _timerText.gameObject.SetActive(false);
        _battleText.gameObject.SetActive(false);
    }

    public void BeginBattleTimer()
    {
        _timerText.gameObject.SetActive(true);
        _battleText.gameObject.SetActive(true);
        StartCoroutine(Timer());

    }

    private IEnumerator Timer()
    {
        for (int i = _duelStartTimer; i > 0; i--)
        {
            _audiosource.PlayOneShot(_tickClip);
            _timerText.text = (i - 1).ToString();

            yield return new WaitForSeconds(1f);
        }
        BeginBattle();
    }

    private void BeginBattle()
    {
        GameControl.allowShoot = true;

        _timerText.gameObject.SetActive(false);
        _battleText.gameObject.SetActive(false);

        _audiosource.clip = _startBattleClip;
        _audiosource.Play();
        if (_isLocal)
        {
            _stopWall.SetActive(false);
        }
    }
}
