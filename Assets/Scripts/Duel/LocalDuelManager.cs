using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(StartDuel))]
public class LocalDuelManager : MonoBehaviour
{
    StartDuel _startDuel;
    [SerializeField] private Transform[] _playerPlaces;
    [SerializeField] private GameObject _bot;
    [SerializeField] private GameObject _player;
    private void Start()
    {
        _startDuel = GetComponent<StartDuel>();
    }
    public void OnColtTake()
    {
        _startDuel.BeginBattleTimer();

        var randomIndex = Random.Range(0, 2);
        _player.transform.position = _playerPlaces[randomIndex].position;
        _player.transform.rotation = _playerPlaces[randomIndex].rotation;

        var botIndex = randomIndex == 1 ? 0 : 1;
        _bot.transform.position = _playerPlaces[botIndex].position;
        _bot.transform.rotation = _playerPlaces[botIndex].rotation;
    }
}
