﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Bhaptics.Tact.Unity;
using Photon.Pun;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(PhotonTransformView))]
public class playerHandler : MonoBehaviour
{
    public bool isVr = false;
    private Transform _spawn;
    private int _playerIndex;
    [SerializeField][Tooltip("VR компоненты")] private GameObject _vrControl;
    [SerializeField] private GameObject[] _notNetworkObjects;
    [SerializeField][Tooltip("скрипт игрока (пока нет нормального)")] private Player _playerControl;
    [SerializeField][Tooltip("No VR компоненты")] private GameObject _noVRControl;
    [SerializeField][Tooltip("Input компоненты")] private GameObject _input;
    [SerializeField][Tooltip("Snap Turn")] private SnapTurn _snap;
    [SerializeField][Tooltip("AudioListener игрока")] private AudioListener _al;
    [SerializeField][Tooltip("Отключаемые части игрока")] private MeshRenderer[] _playerRenderers;
    [SerializeField][Tooltip("Отключаемые скины игрока")] private SkinnedMeshRenderer[] _skin;
    [SerializeField][Tooltip("Части с bhaptic")] private HapticReceiver[] _hapticReceivers;
    private void Start()
    {
        if (!GetComponent<PhotonView>().IsMine)
        {
            MakeNetwork();
        }
    }

    public void MakeLocal(Transform spawnPoint, int index)
    {
        _playerIndex = index;
        _spawn = spawnPoint;
        if (isVr)
        {
            _vrControl.SetActive(true);
        }
        else
        {
            _noVRControl.SetActive(true);
        }
        _playerControl.enabled = true;
        _al.enabled = true;
        _input.SetActive(true);
        _snap.enabled = true;
        for (int i = 0; i < _skin.Length; i++)
        {
            _skin[i].enabled = false;
        }
        for (int i = 0; i < _playerRenderers.Length; i++)
        {
            _playerRenderers[i].enabled = false;
        }
    }

    public void MakeNetwork()
    {
        foreach (GameObject gameObject in _notNetworkObjects)
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                gameObject.transform.GetChild(i).gameObject.SetActive(false);
            }

            foreach (var component in gameObject.transform.GetComponents<Behaviour>())
            {
                print(component);
                if (!(component is PhotonTransformView))
                {
                    component.enabled = false;
                }
            }
        }
        for (var i = 0; i < _hapticReceivers.Count(); i++)
        {
            Destroy(_hapticReceivers[i]);
        }
    }

    public void StartGame()
    {
        gameObject.transform.position = _spawn.position;
        gameObject.transform.rotation = _spawn.rotation;
    }
}
