﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Colt;
using Photon.Pun;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(PhotonTransformView))]
public class PistolHandler : MonoBehaviour
{
    string _owner;
    public int pistol_index;
    [SerializeField] Interactable _interactable;
    [SerializeField] Throwable _throwable;
    //[SerializeField] Colt colt;

    private void Awake()
    {
        _throwable.enabled = false;
    }

    public void spawn(string player, int index)
    {
        _owner = player;
        pistol_index = index;
        Debug.Log(index);
        _interactable.enabled = true;
        _throwable.enabled = true;
        //colt.enabled = true;
    }

    public void take()
    {
        if (_owner == "player")
        {
            PhotonView scene_PV = GameObject.Find("SceneController").GetComponent<PhotonView>();
            scene_PV.RPC("ready", RpcTarget.AllBuffered, pistol_index);
        }
    }
}
