﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR.InteractionSystem;
public class SceneController : MonoBehaviour
{
    public GameObject localPlayer;                              // локальный игрок
    [SerializeField] bool[] _readyPlayers;                      // готовые игроки
    [SerializeField] bool[] _alivePlayers;                      // живые игроки
    [SerializeField] bool _waiting = true;                       // ожидание начала игры
    [SerializeField] bool _gameStarted = false;                 // игра в процессе
    [SerializeField] bool _debugMod = true;
    public bool canRestart = false;
    StartDuel _startDuelScript;
    private bool _isBegin = false;


    private void Start()
    {
        _startDuelScript = GetComponent<StartDuel>();
    }
    void Update()
    {
        if (PhotonNetwork.NetworkClientState == Photon.Realtime.ClientState.Joined)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount > 1)
            {
                // if (waiting)
                // {
                int counter = 0;
                for (int i = 0; i < _readyPlayers.Length - 1; i++)
                {
                    if (_readyPlayers[i] == true)
                    {
                        counter += 1;
                    }
                }
                // we don't allow a single player game to run
                if (counter == PhotonNetwork.CurrentRoom.PlayerCount && counter != 1)
                {
                    _waiting = false;
                    _gameStarted = true;
                    _alivePlayers = _readyPlayers;
                    StartGame();
                }
            }
            //}
        }
        if (_gameStarted)
        {
            checkRestart();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (!_debugMod)
            {
                RestartGame();
            }
        }
    }
    void checkRestart()
    {
        // StartCoroutine(clearScores());
        int counter = 0;
        for (int i = 0; i < _alivePlayers.Length; i++)
        {
            if (_alivePlayers[i] == true)
            {
                counter += 1;
            }
        }
        if (counter == 1)
        {
            canRestart = true;
        }
    }
    IEnumerator WaitAndRestart(float time)
    {
        yield return new WaitForSeconds(time);
        RestartGame();
    }
    public void RestartGame()
    {
        GameControl.allowShoot = false;
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        PhotonNetwork.ConnectUsingSettings();
    }
    IEnumerator clearScores()
    {
        var www = new WWW("http://dreamlo.com/lb/5bE2uizScUiELo9Zc0jRyA6XLjLNGOyEODPnvfQge9vg/clear");
        yield return www;
    }
    [PunRPC]
    public void ready(int index)
    {
        _readyPlayers[index] = true;
    }

    [PunRPC]
    public void dead(int index)
    {
        _alivePlayers[index] = false;
    }

    void StartGame()
    {
        if (!_isBegin)
        {
            _isBegin = true;

            localPlayer.GetComponent<playerHandler>().StartGame();
            // disable music
            gameObject.GetComponent<AudioSource>().Stop();
            // enable damage
            if (_startDuelScript != null)
            {
                _startDuelScript.BeginBattleTimer();
            }
            else
            {
                GameControl.allowShoot = true;
            }
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;
        }
    }

}
