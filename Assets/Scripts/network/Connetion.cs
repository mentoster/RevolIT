using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class Connetion : MonoBehaviourPunCallbacks
{
    [SerializeField] string _appVer = "0.1";
    [SerializeField] byte _maxPlayers = 4;
    [SerializeField] GameObject _player;
    [SerializeField] GameObject _pistol;
    [SerializeField] GameObject _sceneCamera;
    [SerializeField] Transform[] _lobbySpawnPoints;
    [SerializeField] Transform[] _pistolSpawnPoints;
    [SerializeField] Transform[] _shootingSpawnPoints;
    int[] _shootingSpawnIndex;
    [SerializeField] SceneController _sceneController;
    Settings _settings;
    [SerializeField] string _appId = "";
    PhotonView _photonView;
    GameObject _localplayer;

    void Start()
    {
        _shootingSpawnIndex = new int[_shootingSpawnPoints.Length];
        for (var i = 0; i < _shootingSpawnPoints.Length; i++)
        {
            _shootingSpawnIndex[i] = i;
        }

        PhotonNetwork.GameVersion = _appVer;
        _settings = new Settings("", 0);
        if (_settings.serverAddress != "" && _settings.serverPort != 0)
        {
            Debug.Log("Connect to local server");
            PhotonNetwork.PhotonServerSettings.AppSettings.UseNameServer = false;
            PhotonNetwork.PhotonServerSettings.AppSettings.Server = _settings.serverAddress;
            PhotonNetwork.PhotonServerSettings.AppSettings.Port = _settings.serverPort;
        }
        else
        {
            Debug.Log("Connect to remote server");
            PhotonNetwork.PhotonServerSettings.AppSettings.UseNameServer = true;
            PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime = _appId;
            PhotonNetwork.PhotonServerSettings.AppSettings.Server = "";
            PhotonNetwork.PhotonServerSettings.AppSettings.Port = 0;
        }
        PhotonNetwork.ConnectUsingSettings();
        _photonView = gameObject.GetComponent<PhotonView>();
    }
    void SendPositions()
    {
        Shuffle(_shootingSpawnIndex);
        print("I send positions to others");
        _photonView.RPC("GetPositions", RpcTarget.OthersBuffered, _shootingSpawnIndex);
        int players = PhotonNetwork.CurrentRoom.PlayerCount;
        print($"Position name -{_shootingSpawnPoints[_shootingSpawnIndex[players - 1]].name}, i choosed index {_shootingSpawnIndex[players - 1]}");
    }
    [PunRPC]
    void GetPositions(int[] masterPositions)
    {
        print($"71. Connetion ->     masterPositions : { masterPositions}");
        _shootingSpawnIndex = masterPositions;

        int players = PhotonNetwork.CurrentRoom.PlayerCount;
        _localplayer.GetComponent<playerHandler>().MakeLocal(_shootingSpawnPoints[_shootingSpawnIndex[players - 1]], players - 1);
        print($"Position name -{_shootingSpawnPoints[_shootingSpawnIndex[players - 1]].name}, i choosed index {_shootingSpawnIndex[players - 1]}");
    }
    /// <summary>Shuffle input list</summary>
    private void Shuffle<T>(T[] array)
    {
        int n = array.Length;
        for (int i = 0; i < (n - 1); i++)
        {
            int r = i + Random.Range(0, n - i);
            T t = array[r];
            array[r] = array[i];
            array[i] = t;
        }
    }
    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected");
        RoomOptions opt = new RoomOptions();
        opt.MaxPlayers = _maxPlayers;
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("There is no rooms, creating...");
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = _maxPlayers });
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom.");
        var playersObjs = GameObject.FindGameObjectsWithTag("Player");
        int players = PhotonNetwork.CurrentRoom.PlayerCount;

        _localplayer = PhotonNetwork.Instantiate(_player.name, _lobbySpawnPoints[players - 1].position, _lobbySpawnPoints[players - 1].rotation);
        GameObject localpistol = PhotonNetwork.Instantiate(_pistol.name, _pistolSpawnPoints[players - 1].position, _pistolSpawnPoints[players - 1].rotation);
        localpistol.transform.parent = _pistolSpawnPoints[players - 1];
        _sceneController.localPlayer = _localplayer;
        if (_localplayer.GetComponent<PhotonView>().IsMine)
        {
            _localplayer.GetComponent<playerHandler>().MakeLocal(_shootingSpawnPoints[_shootingSpawnIndex[players - 1]], players - 1);
            print($"Position name -{_shootingSpawnPoints[_shootingSpawnIndex[players - 1]].name}, i choosed index {_shootingSpawnIndex[players - 1]}");
            localpistol.GetComponent<PistolHandler>().spawn("player", players - 1);
        }
        _sceneCamera.SetActive(false);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("OnCreatedRoom.");
        SendPositions();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("Payer Connected");
        if (PhotonNetwork.CurrentRoom.PlayerCount == _maxPlayers)
        {
            PhotonNetwork.CurrentRoom.IsVisible = false;
        }
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log(cause);
    }
}
