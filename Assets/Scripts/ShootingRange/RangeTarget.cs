using DG.Tweening;
using UnityEngine;
public class RangeTarget : MonoBehaviour
{
    readonly float _time = 0.4f;
    public bool IsUp = false;
    public void Up()
    {
        IsUp = true;
        transform.parent.DORotate(new Vector3(-90, 0, 0), _time);
    }
    public void ApplyDamage()
    {
        Down();
        IsUp = false;
        transform.parent.parent.SendMessage("TargetDown");
    }

    public void Down() => transform.parent.DORotate(new Vector3(0, 0, 0), _time);
}
