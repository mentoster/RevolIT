using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingRangeManager : MonoBehaviour
{
    [SerializeField] private RangeTarget[] _rangeTargets;
    [SerializeField] private RangeTarget _startRangeTarget;
    private void Start()
    {
        _startRangeTarget.Up();
    }
    public void StartMiniGame()
    {
    }

    public void TargetDown()
    {
        var targetsNow = 0;
        foreach (var target in _rangeTargets)
        {
            if (target.IsUp)
            {
                targetsNow++;
            }
        }
        if (targetsNow <= 0)
        {
            NextTargetUp();
        }
    }

    private void NextTargetUp()
    {
        var chance = Random.Range(0, 100);
        if (chance < 50)
        {
            UpOneTarget();
        }
        else if (chance < 95)
        {
            UpManyTargets();
        }
        else
        {
            UpAllTargets();
        }
    }


    private void UpOneTarget()
    {
        _rangeTargets[Random.Range(0, _rangeTargets.Length - 1)].Up();
    }
    private void UpManyTargets()
    {
        for (var i = 0; i < Random.Range(1, 5); i++)
        {
            _rangeTargets[Random.Range(0, _rangeTargets.Length - 1)].Up();
        }
    }
    private void UpAllTargets()
    {
        foreach (var target in _rangeTargets)
        {
            target.Up();
        }
    }
}
