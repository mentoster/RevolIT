using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FightManager : MonoBehaviour
{
    [SerializeField] TMP_Text _dieText;
    [SerializeField] int _enemies = 3;

    public void CountDie()
    {
        _enemies -= 1;
        if (_enemies <= 0)
        {
            ShowWinText();
        }
    }
    public void PlayerDied()
    {
        ShowDieText();
    }
    private void ShowWinText()
    {
        _dieText.gameObject.SetActive(true);
        _dieText.text = "???????,\n?? ??????? ???? ???????????!";
        StartCoroutine(ReloadScene());

    }
    private void ShowDieText()
    {
        _dieText.gameObject.SetActive(true);
        _dieText.text = "??? ??????????!";

        StartCoroutine(ReloadScene());

    }
    IEnumerator ReloadScene()
    {
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}

