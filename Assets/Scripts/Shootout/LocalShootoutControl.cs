using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class LocalShootoutControl : MonoBehaviour
{

    [Header("BotControl")]
    [SerializeField] private GameObject _botPrefab;
    [Header("Location")]
    [SerializeField] private Transform[] _shootingSpawnPoints;
    private int[] _shootingSpawnIndex;
    [Header("Player")]
    [SerializeField] private GameObject _player;



    private void Start()
    {
        _shootingSpawnIndex = new int[_shootingSpawnPoints.Length];
        for (var i = 0; i < _shootingSpawnPoints.Length; i++)
        {
            _shootingSpawnIndex[i] = i;
        }

        Shuffle(_shootingSpawnIndex);

    }
    public void StartBattle()
    {
        if (!GameControl.allowShoot)
        {
            GameControl.allowShoot = true;
            _player.transform.position = _shootingSpawnPoints[_shootingSpawnIndex[0]].position;
            _player.transform.rotation = _shootingSpawnPoints[_shootingSpawnIndex[0]].rotation;
            for (int i = 1; i < 4; i++)
            {
                var bot = Instantiate(
                      _botPrefab,
                      _shootingSpawnPoints[_shootingSpawnIndex[i]].position,
                      _shootingSpawnPoints[_shootingSpawnIndex[0]].rotation
                      );
                bot.transform.LookAt(_player.transform);
            }
        }
    }

    public void EndBattle()
    {
        GameControl.allowShoot = false;
    }
    /// <summary>Shuffle input list</summary>
    private void Shuffle<T>(T[] array)
    {
        int n = array.Length;
        for (int i = 0; i < (n - 1); i++)
        {
            int r = i + Random.Range(0, n - i);
            (array[i], array[r]) = (array[r], array[i]);
        }
    }
}
