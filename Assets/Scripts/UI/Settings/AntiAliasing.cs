
using System.Globalization;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using TMPro;
using UnityEngine.Rendering;

public class AntiAliasing : MonoBehaviour
{
    [SerializeField] private TMP_Text _scaleNumber;
    [SerializeField] private UniversalRenderPipelineAsset urpAsset;

    private void Start()
    {
        var renderScale = PlayerPrefs.GetFloat("RenderScale");
        var msaa = PlayerPrefs.GetInt("MSAA");

        if (renderScale == null)
        {
            renderScale = 1;
        }
        else
        {
            urpAsset.renderScale = renderScale;
        }

        if (msaa == null)
        {
            msaa = 1;
        }
        else
        {
            urpAsset.msaaSampleCount = msaa;
        }

        _scaleNumber.text = (renderScale).ToString(CultureInfo.CurrentCulture);

    }
    public void OnRenderScale(float scale)
    {
        scale /= 10;
        urpAsset.renderScale = scale;
        _scaleNumber.text = (scale).ToString(CultureInfo.CurrentCulture);
        PlayerPrefs.SetFloat("RenderScale", scale);
    }

    public void OnMSAA(int val)
    {
        switch (val)
        {
            case 0:
                urpAsset.msaaSampleCount = 1;
                break;
            case 1:
                urpAsset.msaaSampleCount = 2;
                break;
            case 2:
                urpAsset.msaaSampleCount = 4;
                break;
            case 3:
                urpAsset.msaaSampleCount = 8;
                break;
        }
        PlayerPrefs.SetInt("MSAA", urpAsset.msaaSampleCount);
    }
}
