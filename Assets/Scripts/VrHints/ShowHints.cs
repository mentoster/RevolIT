﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class ShowHints : MonoBehaviour
{
    PhotonView PV;

    [SerializeField] SteamVR_Action_Boolean _hintShoot;
    [SerializeField] SteamVR_Action_Boolean _hintReload;
    [SerializeField] GameObject _handsParent;
    [SerializeField] Hand _leftHand;
    [SerializeField] Hand _rightHand;
    [SerializeField] bool _showHints = true;
    // Start is called before the first frame update
    void Start()
    {
        PV = gameObject.GetComponent<PhotonView>();
        StartCoroutine(ShowHintsToPlayer());
    }

    IEnumerator ShowHintsToPlayer()
    {
        yield return new WaitForSeconds(1f);
        if (_showHints && PV.IsMine)
        {
            _leftHand.ShowShootHint();
            _leftHand.ShowReloadHint();
            _rightHand.ShowShootHint();
            _rightHand.ShowReloadHint();
        }
    }

    private void Update()
    {
        // if we press buttons, we disable hints
        if (_showHints && PV.IsMine)
        {
            if (_hintShoot.GetStateDown(SteamVR_Input_Sources.RightHand) || _hintShoot.GetStateDown(SteamVR_Input_Sources.LeftHand))
            {
                _leftHand.HideShootHint();
                _rightHand.HideShootHint();
            }
            else if (_hintReload.GetStateDown(SteamVR_Input_Sources.RightHand) || _hintReload.GetStateDown(SteamVR_Input_Sources.LeftHand))
            {

                _leftHand.HideReloadHint();
                _rightHand.HideReloadHint();

            }
            if (Input.GetKeyDown(KeyCode.H))
            {
                StartCoroutine(ShowHintsToPlayer());
            }
        }

    }
}
