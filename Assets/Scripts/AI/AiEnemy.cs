using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Animations.Rigging;
using static UnityEngine.GraphicsBuffer;

public class AiEnemy : MonoBehaviour
{

    [Header("Attributes")]
    [SerializeField] private LocalColt _ownColt;
    [SerializeField] private GameObject _player;
    [SerializeField] private MultiAimConstraint _bodyRig;
    [SerializeField] private Transform _shootPoint;
    private LocalColt _playerColt;
    private float _currentWeight = 1.0f;
    private RigBuilder _rigBuilder;
    private GameObject _target;
    private Animator _animator;

    [Header("Bot Parameters")]
    [SerializeField] private int _health = 250;
    [SerializeField] private int _applyDamage = 30;
    private readonly float _maxShootOffset = 0.3f;


    #region otherParams
    private CapsuleCollider _capsuleCollider;
    private bool _isSitting = false;
    private bool _isDead = false;
    #endregion


    private void Start()
    {
        _animator = GetComponent<Animator>();
        _capsuleCollider = GetComponent<CapsuleCollider>();
        _rigBuilder = GetComponent<RigBuilder>();

        _playerColt = GameObject.Find("LocalColt").GetComponent<LocalColt>();
        _player = GameObject.Find("FallbackObjects");

        _target = Instantiate(new GameObject());
        _target.transform.parent = _player.transform;
        _target.transform.localPosition = Vector3.zero;

        SetRigTarger(_target.transform);
    }

    #region Rigging
    private void SetRigTarger(Transform target)
    {

        _bodyRig.data.sourceObjects = new WeightedTransformArray { new WeightedTransform(target, 1) };
        _rigBuilder.enabled = false;
        _rigBuilder.enabled = true;
    }

    #endregion
    #region LifeMethods
    public void ApplyDamage()
    {
        if (!_isSitting && !_isDead)
        {
            if (GameControl.allowShoot)
            {
                _health -= _applyDamage;
                if (_health <= 0)
                {
                    Die();
                    _playerColt.UpdateScore(true, _applyDamage);
                }
                else
                {
                    _playerColt.UpdateScore(false, _applyDamage);
                }
                // chance to hide
                if (Random.Range(0, 4) > 3)
                {
                    SitAndReload();
                }
            }
            else
            {
                _playerColt.UpdateScore(false, 0);
            }
        }
    }
    private void Die()
    {
        DOTween.To(() => _currentWeight, x => _currentWeight = x, 0, 0.4f);
        _player.transform.root.GetComponent<FightManager>().CountDie();
        _animator.SetTrigger("Die");
        _isDead = true;
    }
    #endregion
    #region Shooting
    public void OnShoot()
    {
        if (_ownColt.bullets > 0)
        {
            if (IsDirectFire())
            {
                _animator.SetFloat("ShootSpeed", Random.Range(0.7f, 1.3f));

                _ownColt.Shoot();
                // add random shoot offset
                _target.transform.DOLocalMove(AddRandomShootOffset(), 0.3f);
            }
            else
            {
                _animator.SetBool("Stand", true);
            }
        }
        else
        {
            SitAndReload();
        }
    }
    public void OnCheckDirectFire()
    {
        if (IsDirectFire())
        {
            _animator.SetBool("Stand", false);
        }
    }
    public bool IsDirectFire()
    {
        return !Physics.Linecast(_shootPoint.position, _target.transform.position);
    }
    private Vector3 AddRandomShootOffset()
    {
        return new Vector3(
          Random.Range(-_maxShootOffset, _maxShootOffset),
          Random.Range(-_maxShootOffset, _maxShootOffset),
          Random.Range(-_maxShootOffset, _maxShootOffset)
        );
    }
    private void SitAndReload()
    {
        DOTween.To(() => _currentWeight, x => _currentWeight = x, 0, 0.4f);

        _animator.SetTrigger("Sit");
        _ownColt.Reload();

        _isSitting = true;
    }
    public void Up()
    {
        DOTween.To(() => _currentWeight, x => _currentWeight = x, 1, 0.4f);
        _isSitting = false;
    }

    #endregion
    private void Update()
    {
        _bodyRig.weight = _currentWeight;
    }
}
