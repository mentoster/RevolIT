using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class SetToUI : MonoBehaviour
{
    [SerializeField] GameObject _stroke;
    public void SetHighscore(List<Highscore> highscoresList)
    {

        // sort our list for scores
        highscoresList.Sort(Highscore.HighscoreCompareScore);
        highscoresList.Reverse();

        // show max 5 points
        int length = (highscoresList.Count > 7) ? 7 : highscoresList.Count;
        for (var i = 0; i < length; i++)
        {
            var str = Instantiate(_stroke);
            str.transform.parent = gameObject.transform;

            str.gameObject.GetComponent<Row>().SetParameters(i + 1, highscoresList[i]);
            str.transform.DOLocalRotate(new Vector3(0, -90, 0), 0.01f);
            str.transform.localPosition = new Vector3(0.7f, 0.05f - 0.05f * i, -0.256f);
            str.transform.localScale = new Vector3(0.02f, 0.01f, 1);
        }
    }
}
