
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Networking;

public class LeaderBoard : MonoBehaviour
{
    const string _publicCode = "60d4758b8f40bb114c4fadb4";
    const string _webURL = "http://dreamlo.com/lb/";
    public List<Highscore> highscoresList = new List<Highscore>();
    [SerializeField] string _privateCode;
    [SerializeField] SetToUI[] _tables;


    void Awake()
    {

        if (_privateCode != null)
        {
            // AddNewHighscore("Первый экспертный 02.10.2021 15:34:14", 200);
            // AddNewHighscore("kriper_2004", 500);
            DownloadHighscores();
        }
        else
        {
            Debug.LogAssertion("🔑You didn't set api key for score table!");
        }
    }
    public void AddNewHighscore(string username, int score)
    {
        StartCoroutine(UploadNewHighscore(username, score));
    }
    IEnumerator UploadNewHighscore(string username, int score)
    {
        var www = new WWW(_webURL + _privateCode + "/add/" + WWW.EscapeURL(username) + "/" + score);
        yield return www;
        if (string.IsNullOrEmpty(www.error))
        {
            print("Upload score Successful!");
        }
        else
        {
            print("Upload score Failed: " + www.error);
        }
    }
    public void DownloadHighscores()
    {
        StartCoroutine(DownloadHighscoresFromDatabase());
    }
    IEnumerator DownloadHighscoresFromDatabase()
    {
        yield return new WaitForSeconds(Random.Range(0, 5));
        var www = new WWW(_webURL + _publicCode + "/pipe/");
        yield return www;
        if (string.IsNullOrEmpty(www.error))
        {
            FormatHighscores(www.text);
            SetToUI();
        }
        else
        {
            print("Upload Failed: " + www.error);
        }
    }
    void FormatHighscores(string textInput)
    {
        string[] entries = textInput.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < entries.Length; i++)
        {
            string[] entryInfo = entries[i].Split(new char[] { '|' });
            string username = entryInfo[0];
            int score = int.Parse(entryInfo[1]);
            highscoresList.Add(new Highscore(username, score));
        }
    }

    ///<summary>
    ///Set list of scores to ui unity
    ///</summary>
    void SetToUI()
    {
        foreach (var table in _tables)
        {
            table.SetHighscore(highscoresList);
        }
    }
}
public struct Highscore
{
    public string username;
    public int score;

    public Highscore(string _username, int _score)
    {
        username = _username;
        score = _score;
    }
    static public int HighscoreCompareScore(Highscore hs1, Highscore hs2)
    {
        return hs1.score.CompareTo(hs2.score);
    }

}
