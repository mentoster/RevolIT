using TMPro;
using UnityEngine;

public class Row : MonoBehaviour
{
    [SerializeField] TMP_Text _rank;
    [SerializeField] TMP_Text _playerName;
    [SerializeField] TMP_Text _score;
    /// <summary>
    /// Set score parameters to stroke
    ///</summary>
    public void SetParameters(int rank, Highscore highscore)
    {
        _rank.text = rank.ToString();
        _playerName.text = highscore.username;
        _score.text = highscore.score.ToString();
    }
}
