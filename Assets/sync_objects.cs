﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class sync_objects : MonoBehaviour
{
    [PunRPC]
    public void Sync(float x, float y, float z)
    {
        gameObject.transform.position = new Vector3(x, y, z);
    }
}
