<p align="center">
  <a href="" rel="noopener">
 <img height=200px src="Assets/Logo.jpg" alt="Project logo"></a>
</p>

<h3 align="center">RevolIT</h3>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![GitHub Issues](https://img.shields.io/github/issues/RTUITLab/RevolIT.svg)](https://github.com/RTUITLab/RevolIT/issues)
[![GitHub Pull Requests](https://img.shields.io/github/issues-pr/RTUITLab/RevolIT.svg)](https://github.com/kylelobo/The-Documentation-Compendium/pulls)

</div>

---

<p align="center"> Вестерн в виртуальной реальности c использованием bhaptic.

</p>

## 📝 Table of Contents

* [О проекте](#about)
* [Запуск проекта для разработки](#getting_started)
* [Запуск рабочего проектаа](#build)
* [Управление](#control)
* [TODO](TODO.md)
* [Приняли участие](#authors)
* [Благодарности](#acknowledgement)

## 🧐 О проекте <a name = "about"></a>

Revolit - это онлайн игра, рассчитанная на 4 человек.
В игре вы сражаетесь в салуне за право считаться достойным ковбоем.

<!-- TODO: вставить видео игры -->

## 🔨 Запуск проекта для разработки <a name = "getting_started"></a>

1. Клонировать репозиторий
2. Установить Unity 2020.3.27f1.
3. Открыть проект через unityhub.

## 🏁 Запуск рабочего проекта <a name = "build"></a>

1. Открыть [релизы](https://github.com/RTUITLab/RevolIT/releases)
2. Скачать последний релиз, есть hd (есть постпроцессинг) версия, её нужно скачать первым делом, если она подвисает, то обычную.
3. Запустить проект
4. Для сетевой игры, нужно открыть проект на нескольких компьютерах, клиенты соединятся самостоятельно.
5. Для старта игры, всем игрокам необходимо взять в руки револьвер.

В случае, если игрок не смог подключится, нажмите кнопку R, для переподключения.

## ⌨ Управление <a name = "control"></a>

| Кнопка        | Действие           |
| ------------- |:------------------:|
|![1](https://github.com/q2apro/keyboard-keys-speedflips/blob/master/single-keys-blank/200dpi/r.png?raw=true)   | Перезапустить игру у клиента |
|![1](https://github.com/q2apro/keyboard-keys-speedflips/blob/master/single-keys-blank/200dpi/h.png?raw=true)   | Показать подсказки снова |

## ✍️ Приняли участие <a name = "authors"></a>

* [@leonis13579](https://github.com/leonis13579) - Идея & работа с unity
* [@mentoster](https://github.com/mentoster) - работа с unity
* [@Delivery-Klad](https://github.com/Delivery-Klad) - работа с unity и сценой
* [@ishoshnikov](https://vk.com/ishoshnikov) - работа с моделями

## 🎉 Благодарности <a name = "acknowledgement"></a>

* Спасибо RTUitLab за предоставленное оборудование.
